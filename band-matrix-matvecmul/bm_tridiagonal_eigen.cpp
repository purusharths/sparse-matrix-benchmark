#include <benchmark/benchmark.h>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/src/Core/Matrix.h>
#include <eigen3/Eigen/src/Core/util/Constants.h>
#include <iostream>

using namespace Eigen;

#define N 20000

int rows = N;
int cols = N;

SparseMatrix<double, RowMajor> sp_diag_mat(rows, cols);
MatrixXd mat = MatrixXd::Random(rows, cols);
VectorXd v = VectorXd::LinSpaced(N, 1, 10), vec5(N, 1);

void create_tridiagonal_matrix(SparseMatrix<double, RowMajor> &sA) {
  for (int i = 0; i < N; i++) {
    float v_ij = 5.5; // 10*dist(gen);
    sA.insert(i, i) = v_ij;
    if (i + 1 < N) {
      sA.insert(i, i + 1) = v_ij;
    }
    if (i > 0) {
      sA.insert(i, i - 1) = v_ij;
    }
  }
  sA.makeCompressed();
}

void SparseDiagVectorMult() { vec5 = sp_diag_mat * v; }

void DenseVectorMult() { vec5 = mat * v; }

static void DiagSparseVec(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    SparseDiagVectorMult();
  }
}

static void DenseVector(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    DenseVectorMult();
  }
}

BENCHMARK(DenseVector);
BENCHMARK(DiagSparseVec);

int main(int argc, char **argv) {
  create_tridiagonal_matrix(sp_diag_mat);
  ::benchmark::Initialize(&argc, argv);
  ::benchmark::RunSpecifiedBenchmarks();
}
