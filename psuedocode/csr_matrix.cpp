#include <benchmark/benchmark.h>
#include <exception>
#include <iostream>
#include <vector>

void CSR_vec_mult() {
  std::vector<double> AA = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
  std::vector<double> IC = {0, 3, 0, 1, 3, 0, 2, 3, 4, 2, 3, 4};
  std::vector<double> IA = {0, 2, 5, 9, 11};
  std::vector<double> B = {1, 2, 3, 4, 5};
  std::vector<double> result(B.size());

  for (size_t index = 0; index < IA.size(); index++) {
    double row_first_element = IA[index];
    double row_last_elemnet, sum = 0;
    if (index + 1 > IA.size()) {
      row_last_elemnet = IA[index];
    } else {
      row_last_elemnet = IA[index + 1] - 1;
    }
    if (row_last_elemnet < 0)
      row_last_elemnet = IC.size() - 1;
    for (int i = row_first_element; i <= row_last_elemnet; i++) {
      sum += AA[i] * B[IC[i]];
    }
    // std::cout << "SUM: " << sum << std::endl;
    result[index] = sum;
  }
}

void NormalMultiplicationSparse() {
  std::vector<std::vector<double>> mat = {{1, 0, 0, 2, 0},
                                          {3, 4, 0, 5, 0},
                                          {6, 0, 7, 8, 9},
                                          {0, 0, 10, 11, 0},
                                          {0, 0, 0, 0, 12}};
  std::vector<double> B = {1, 2, 3, 4, 5};
  std::vector<double> result(B.size());
  for (int i = 0; i < mat[0].size(); i++) {
    double sum = 0;
    for (int j = 0; j < mat[i].size(); j++) {
      sum += mat[i][j] * B[j];
    }
    result.emplace_back(sum);
  }
}

void NormalMultiplicationDense() {
  std::vector<std::vector<double>> mat = {{1, 50, 10, 2, 30},
                                          {3, 4, 60, 5, 40},
                                          {6, 50, 7, 8, 9},
                                          {90, 30, 10, 11, 60},
                                          {80, 70, 30, 30, 12}};
  std::vector<double> B = {1, 2, 3, 4, 5};
  std::vector<double> result(B.size());
  for (int i = 0; i < mat[0].size(); i++) {
    double sum = 0;
    for (int j = 0; j < mat[i].size(); j++) {
      sum += mat[i][j] * B[j];
    }
    result.emplace_back(sum);
  }
}

static void Sparse_vecmult_with_CSR_ordering(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    CSR_vec_mult();
  }
}

static void Sparse_vecmult_general_ordering(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    NormalMultiplicationSparse();
  }
}

static void Dense_vecmult_general_ordering(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    NormalMultiplicationDense();
  }
}




BENCHMARK(Sparse_vecmult_with_CSR_ordering);
BENCHMARK(Dense_vecmult_general_ordering);
BENCHMARK(Sparse_vecmult_general_ordering);
BENCHMARK_MAIN();