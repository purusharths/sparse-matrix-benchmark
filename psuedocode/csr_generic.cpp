#include "convert_to_csr.hh"
#include <benchmark/benchmark.h>
#include <iostream>
#include <string>
#include <vector>

std::vector<std::vector<double>> mat;
std::vector<double> IA;
std::vector<double> IC;
std::vector<double> AA;
std::vector<double> result;
std::vector<double> B;

double sparsity;

void CSR_vec_mult() {

  // delete mat
  //   for (int i = 0; i < N; i++) {
  //     for (int j = 0; j < N; j++) {
  //       std::cout << mat[i][j] << " ";
  //     }
  //     std::cout << std::endl;
  //   }

  for (size_t index = 0; index < IA.size(); index++) {
    double row_first_element = IA[index];
    double row_last_elemnet, sum = 0;
    if (index + 1 > IA.size()) {
      row_last_elemnet = IA[index];
    } else {
      row_last_elemnet = IA[index + 1] - 1;
    }
    if (row_last_elemnet < 0)
      row_last_elemnet = IC.size() - 1;
    for (int i = row_first_element; i <= row_last_elemnet; i++) {
      sum += AA[i] * B[IC[i]];
    }
    // std::cout << "SUM: " << sum << std::endl;
    result[index] = sum;
  }
}

std::vector<std::vector<double>> dense_mat;
std::vector<double> dense_v;
std::vector<double> dense_results;
void NormalMultiplicationDense() {

  for (int i = 0; i < dense_mat[0].size(); i++) {
    double sum = 0;
    for (int j = 0; j < dense_mat[i].size(); j++) {
      sum += mat[i][j] * B[j];
    }
    result.emplace_back(sum);
  }
}

static void CSR_VECMULT(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    CSR_vec_mult();
  }
}

static void DENSE_VECMULT(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    NormalMultiplicationDense();
  }
}

BENCHMARK(CSR_VECMULT);
BENCHMARK(DENSE_VECMULT);

int main(int argc, char **argv) {
  int N = std::stoi(argv[1]);
  IA.resize(N);
  mat.resize(N, std::vector<double>(N, 0));
  dense_mat.resize(N, std::vector<double>(N, 0));
  B.resize(N, 1);
  dense_v.resize(N,1);
  result.resize(B.size());
  dense_results.resize(dense_v.size());
  create_random_sparse_matrix(mat, N, N, sparsity);
  compress_matrix(mat, AA, IC, IA, N);
  create_random_sparse_matrix(dense_mat, N, N, sparsity, 1.0);

  //   CSR_vec_mult(N, result);
  ::benchmark::Initialize(&argc, argv);
  ::benchmark::RunSpecifiedBenchmarks();
}