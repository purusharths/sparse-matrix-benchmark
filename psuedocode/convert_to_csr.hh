#include <cstdlib>
#include <iostream>
#include <random>
#include <vector>

void create_random_sparse_matrix(std::vector<std::vector<double>> &mat,
                                 int rows, int cols, double &sparsity, float sp_percent = 0.4) {
  std::default_random_engine gen;
  std::uniform_real_distribution<double> dist(0.0, 1.0);
  int non_zero_entires;

  for (int i = 0; i < rows; ++i) {
    int update_flag = 0;
    for (int j = 0; j < cols; ++j) {
      auto v_ij = dist(gen); // generate random number
      if (v_ij < sp_percent) {
        mat[i][j] = std::ceil(10 * v_ij);
        non_zero_entires++;
        update_flag++;
      }
    }
    if (update_flag == 0) {
      mat[i][cols - 1] = 1;
    }
  }
  // add to the matrix
  sparsity = ((double)non_zero_entires) / (rows * cols);
}

void compress_matrix(std::vector<std::vector<double>> &mat,
                     std::vector<double> &aa, std::vector<double> &ic,
                     std::vector<double> &ia, int N) {
  for (int i = 0; i < N; i++) {
    int first_non_zero = 0;

    for (int j = 0; j < N; j++) {
      if (mat[i][j] != 0) {
        aa.emplace_back(mat[i][j]);
        ic.emplace_back(j);

        if (first_non_zero == 0) {
        //   std::cout << ic.size() << std::endl;
          ia[i] = ic.size() - 1;
          first_non_zero++;
        }
      }
      if (j + 1 == N && first_non_zero == 0)
        ia[i] = -1;
    }
  }
}

// int main(int argc, char **argv) {
//   int N = std::atoi(argv[1]);
//   std::vector<std::vector<double>> mat(N, std::vector<double>(N, 0));
//   std::vector<double> ia(N);
//   std::vector<double> ic;
//   std::vector<double> aa;
//   double sparsity;

//   // fill sparse matrix.
//   create_random_sparse_matrix(mat, N, N, sparsity);
//   compress_matrix(mat, aa, ic, ia, N);

  //   for (int i = 0; i < N; i++) {
  //     for (int j = 0; j < N; j++) {
  //       std::cout << mat[i][j] << " ";
  //     }
  //     std::cout << std::endl;
  //   }
  //   std::cout << "\n\n";
  //   for (int i = 0; i < N; i++) {
  //     std::cout << aa[i] << ", ";
  //   }
  //   std::cout << "\n\n";

  //   for (int i : ic) {
  //     std::cout << i << ", ";
  //   }
  //   std::cout << "\n\n";

  //   for (int i = 0; i < N; i++) {
  //     std::cout << ia[i] << ", ";
  //   }
// }