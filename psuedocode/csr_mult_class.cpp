#include <iostream>
#include <vector>

class SparseMatMul {
private:
  std::vector<double> &AA;
  std::vector<double> &IC;
  std::vector<double> &IA;
  std::vector<double> &B;
  std::vector<double> result;

public:
  SparseMatMul(std::vector<double> &AA_, std::vector<double> &IC_,
               std::vector<double> &IA_, std::vector<double> &B_)
      : AA(AA_), IC(IC_), IA(IA_), B(B_) {
    result.resize(sizeof(B));
  }

  void calculate_dot_product() {
    for (size_t index = 0; index < IA.size(); index++) {
      double row_first_element = IA[index];
      double row_last_elemnet, sum = 0;
      if (index + 1 > IA.size()) {
        row_last_elemnet = IA[index];
      } else {
        row_last_elemnet = IA[index + 1] - 1;
      }
      if (row_last_elemnet < 0)
        row_last_elemnet = IC.size() - 1;
      for (int i = row_first_element; i <= row_last_elemnet; i++) {
        sum += AA[i] * B[IC[i]];
      }
      result[index] = sum;
    }
  }

  std::vector<double> &getResult() { return result; } // will get destroyed once class' scope end.
};