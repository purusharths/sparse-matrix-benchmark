#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/IterativeLinearSolvers>
#include <eigen3/Eigen/Sparse>
#include <eigen3/unsupported/Eigen/IterativeSolvers>
#include <iostream>
#include <random>

#include <benchmark/benchmark.h>
#include <string>

using namespace Eigen;
typedef Eigen::Triplet<double> T;

static const int n = 10;
static const float sp_percent = 0.8;

void create_random_sparse_matrix(SparseMatrix<double> &mat, int rows,
                                 int cols) {
  std::default_random_engine gen;
  std::uniform_real_distribution<double> dist(0.0, 1.0);

  std::vector<Eigen::Triplet<double>> tripletList;
  for (int i = 0; i < rows; ++i) {
    for (int j = 0; j < cols; ++j) {
      auto v_ij = dist(gen); // generate random number
      if (v_ij < sp_percent) { // sparsity %
        tripletList.push_back(
            T(i, j, v_ij)); // if larger than treshold, insert it
      }
    }
  }
  mat.setFromTriplets(tripletList.begin(), tripletList.end());
  // mat = Eigen::MatrixXd::Random(n, n).sparseView(0.2, 1);
  for (int i = 0; i < n; i++) {
    float v_ij = 5.5;         // 10*dist(gen);
    mat.coeffRef(i, i) = 100; // ensure diagonal dominant matrices
  }
  mat.makeCompressed();
}

Eigen::SparseMatrix<double> S(n, n);
// = Eigen::MatrixXd::Random(n, n).sparseView(0.5, 1);
Eigen::VectorXd b(n), x;

// Declaration of objects
Eigen::ConjugateGradient<SparseMatrix<double>, Eigen::Lower | Eigen::Upper,
                         Eigen::IdentityPreconditioner>
    cg;
Eigen::GMRES<SparseMatrix<double>, Eigen::IdentityPreconditioner> gmres;

Eigen::MINRES<SparseMatrix<double>, Eigen::Lower | Eigen::Upper,
              Eigen::IdentityPreconditioner>
    minres;
Eigen::BiCGSTAB<SparseMatrix<double>, Eigen::IdentityPreconditioner> bicg;
Eigen::DGMRES<SparseMatrix<double>, Eigen::IdentityPreconditioner> dgmres;
Eigen::SparseLU<SparseMatrix<double>, COLAMDOrdering<int>> solver;

void bm_cg() {
  cg.compute(S);
  x = cg.solve(b);
}
void bm_gmres() {
  gmres.compute(S);
  x = gmres.solve(b);
}
void bm_minres() {

  minres.compute(S);
  x = minres.solve(b);
}
void bm_BiCGSTAB() {
  bicg.compute(S);
  x = bicg.solve(b);
}
void bm_dgmres() {
  dgmres.compute(S);
  x = dgmres.solve(b);
}

void bm_direct_LU() {
  solver.analyzePattern(S);
  solver.factorize(S);
  x = solver.solve(b);
}

static void BM_CG(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    bm_cg();
  }
}

static void BM_GMRES(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    bm_gmres();
  }
}

static void BM_MINRES(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    bm_minres();
  }
}

static void BM_JACOBI(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    bm_BiCGSTAB();
  }
}
static void BM_DGMRES(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    bm_dgmres();
  }
}
static void BM_DIRECT_LU(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    bm_direct_LU();
  }
}
// -----------------
BENCHMARK(BM_CG);
BENCHMARK(BM_GMRES);
BENCHMARK(BM_MINRES);
BENCHMARK(BM_JACOBI);
BENCHMARK(BM_DGMRES);
BENCHMARK(BM_DIRECT_LU);

int main(int argc, char **argv) {
  b.setRandom();
  //   n = std::stoi(argv[1]);
  create_random_sparse_matrix(S, n, n);
  // std::cout<< S;
  ::benchmark::Initialize(&argc, argv);
  ::benchmark::RunSpecifiedBenchmarks();
  double sparsity = ((double)S.nonZeros() / (n * n)) * 100;
  std::cout << "Matrix Size: " << n << std::endl;
  std::cout << "Sparsity: " << sparsity << std::endl;

  // error estimates
  std::cout << "\nGMRES:" << gmres.error() << std::endl;
  std::cout << "MINRES:" << minres.error() << std::endl;
  std::cout << "CG:" << cg.error() << std::endl;
  std::cout << "bicg:" << bicg.error() << std::endl;
  std::cout << "DGMRES:" << dgmres.error() << std::endl;
}

/**
 * @brief A diagonally dominant matrix is guaranteed to have either all positive (if the entries of the diagonal are all positive)
  or all negative (if the entries are all negative) eigenvalues, by Gershgorin's theorem. Most iterative methods only work if the eigenvalues
   of the iteration matrix are in a particular region of the complex plane, so diagonal dominance ensures that all of the eigenvalues have either
    a stricly positive or strictly negative real part (or that all the eigenvalues lie within a particular radius of some number).

If your iteration matrix has eigenvalues which lie outside the prescribed region for a particular method, it usually won't work correctly. 
There are piles of methods out there, but I can't pick a specific method out without knowing more about the spectrum of 
 * 
 */