#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Sparse>
#include <eigen3/Eigen/src/Core/util/Constants.h>
#include <iostream>
#include <random>

#include <benchmark/benchmark.h>

using namespace Eigen;

typedef Eigen::Triplet<double> T;

 #define N 20000

SparseMatrix<double, RowMajor> mat(N, N);
MatrixXd dense = MatrixXd::Random(N, N);
VectorXd v4(N, 1), v5(N);
double sparsity;


void create_random_sparse_matrix(SparseMatrix<double, RowMajor> &mat,
                                        int rows, int cols) {
  std::default_random_engine gen;
  std::uniform_real_distribution<double> dist(0.0, 1.0);
  int non_zero_entires;

  std::vector<Eigen::Triplet<double>> tripletList;
  for (int i = 0; i < rows; ++i) {
    for (int j = 0; j < cols; ++j) {
      auto v_ij = dist(gen); // generate random number
      if (v_ij < 0.1) {
        tripletList.push_back(
            T(i, j, v_ij)); // if larger than treshold, insert it
        non_zero_entires++;
      }
    }
  }
  // add to the matrix
  sparsity = ((double)non_zero_entires) / (rows*cols);
  mat.setFromTriplets(tripletList.begin(), tripletList.end());
  mat.makeCompressed();
}

void mult_sparse() { v5 = mat * v4; }

void mult_Dense() { v5 = dense * v4; }

static void SparseVec(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    mult_sparse();
  }
}

static void DenseVector(benchmark::State &state) {
  std::vector<double> result;
  for (auto _ : state) {
    mult_Dense();
  }
}

BENCHMARK(DenseVector);
BENCHMARK(SparseVec);

int main(int argc, char **argv) {
  create_random_sparse_matrix(mat, N, N);
  ::benchmark::Initialize(&argc, argv);
  ::benchmark::RunSpecifiedBenchmarks();
  std::cout << "Sparsity: " << sparsity*100 << "% " << std::endl;
  
}
